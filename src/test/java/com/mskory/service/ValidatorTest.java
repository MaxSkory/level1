package com.mskory.service;

import com.mskory.exceptions.NumericOverflowException;
import com.mskory.exceptions.TypeAccordanceException;
import com.mskory.exceptions.ValuesMagnitudeOrderException;
import com.mskory.exceptions.IncrementNotPositiveException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class ValidatorTest {

    private static final BigDecimal[] MAX_VALUES = new BigDecimal[]{
            BigDecimal.valueOf(Integer.MAX_VALUE),
            BigDecimal.valueOf(Double.MAX_VALUE),
            BigDecimal.valueOf(Float.MAX_VALUE),
            BigDecimal.valueOf(Long.MAX_VALUE),
            BigDecimal.valueOf(Short.MAX_VALUE)};

    private static final BigDecimal NUMBER = BigDecimal.valueOf(2);
    private static final String NUMBERS_TYPE = "int";
    private static final String BAD_FORMAT = "1.3jk";
    private static final String DECIMAL = "1.1";
    private static final String MIN = "1";
    private static final String MAX = "100";
    private static final String INCREMENT = "2";
    private static final String ZERO = "0";
    private static final String NEGATIVE = "-1";
    private final Validator validator = new Validator(NUMBERS_TYPE, MIN, MIN, MIN, MAX_VALUES[1]);

    @BeforeEach
    void setUp() {
        validator.setMin(MIN)
                .setMax(MAX)
                .setIncrement(INCREMENT)
                .setNumberType(NUMBERS_TYPE)
                .setMaxValue(MAX_VALUES[0]);
    }

    @Test
    void nullCheck_nullValue_notOk() {
        validator.setIncrement(null);
        assertThrows(NullPointerException.class, validator::validate);
        validator.setIncrement(MIN).setMax(null);
        assertThrows(NullPointerException.class, validator::validate);
        validator.setMax(MIN).setMin(null);
        assertThrows(NullPointerException.class, validator::validate);
    }

    @Test
    void nullCheck_validValues_Ok() {
        assertTrue(validator.validate());
    }

    @Test
    void magnitudeOrderCheck_minGreaterMax_notOk() {
        validator.setMax(ZERO);
        assertThrows(ValuesMagnitudeOrderException.class, validator::validate);
    }

    @Test
    void magnitudeOrderCheck_minLessEqualsMax_Ok() {
        assertTrue(validator.validate());
        validator.setMin(ZERO);
        assertTrue(validator.validate());
    }

    @Test
    void zeroIncrementCheck_incrementZero_notOk() {
        validator.setIncrement(ZERO);
        assertThrows(IncrementNotPositiveException.class, validator::validate);
    }

    @Test
    void zeroIncrementCheck_lessThanZeroIncrement_notOk() {
        validator.setIncrement(NEGATIVE);
        assertThrows(IncrementNotPositiveException.class, validator::validate);
    }

    @Test
    void zeroIncrementCheck_greaterThanZeroIncrement_Ok() {
        assertTrue(validator.validate());
    }

    @Test
    void typeAccordanceCheck_numbersDecimalTypeInteger_notOk() {
        validator.setMax(MAX).setMin(DECIMAL);
        assertThrows(TypeAccordanceException.class, validator::validate);
        validator.setMin(MIN).setMax(DECIMAL);
        assertThrows(TypeAccordanceException.class, validator::validate);
        validator.setMax(MIN).setIncrement(DECIMAL);
        assertThrows(TypeAccordanceException.class, validator::validate);
    }

    @Test
    void typeAccordanceCheck_numbersDecimalTypeDecimal_Ok() {
        validator.setMaxValue(MAX_VALUES[1])
                .setMax(DECIMAL)
                .setMin(DECIMAL)
                .setIncrement(DECIMAL);
        assertTrue(validator.validate());
    }

    @Test
    void numbersFormatCheck_inputNotNumber_notOk() {
        validator.setMin(BAD_FORMAT);
        assertThrows(NumberFormatException.class, validator::validate);
        validator.setMin(MIN).setMax(BAD_FORMAT);
        assertThrows(NumberFormatException.class, validator::validate);
        validator.setMax(MAX).setIncrement(BAD_FORMAT);
        assertThrows(NumberFormatException.class, validator::validate);
    }

    @Test
    void overflowCheck_maxValuesArguments_notOk() {
        for (BigDecimal maxValue : MAX_VALUES) {
            validator.setMaxValue(maxValue);
            assertThrows(NumericOverflowException.class, () -> validator.overflowCheck(maxValue, maxValue));
            assertThrows(NumericOverflowException.class, () -> validator.overflowCheck(NUMBER, maxValue));
        }
    }
}