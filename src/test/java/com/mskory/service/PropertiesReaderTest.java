package com.mskory.service;

import com.mskory.exceptions.PropertiesNotFoundException;
import com.mskory.service.service.LogAppender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class PropertiesReaderTest {

    public static final String VALID_FILE_NAME = ".properties";
    public static final String INVALID_FILE_NAME = "ABC.properties";
    private final PropertiesReader reader = new PropertiesReader(VALID_FILE_NAME);
    private LogAppender logAppender;

    @BeforeEach
    void setUp() {
        reader.setFileName(VALID_FILE_NAME);
    }

    @Test
    void getJarProperties_fileNotExist_notOk() {
        reader.setFileName(INVALID_FILE_NAME);
        assertThrows(PropertiesNotFoundException.class, reader::getProperties);
    }

    @Test
    void getJarProperties_fileExists_Ok() {
        assertEquals(reader.getProperties().getClass(), Properties.class);
    }
}