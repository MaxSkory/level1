package com.mskory.service;

import com.mskory.exceptions.NoSuchElementException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MultiplyingTableServiceSupplierTest {
    private static final String invalidType = "Integer";

    private final MultiplyingTableServiceSupplier supplier = new MultiplyingTableServiceSupplier();
    @Test
    void getTable_invalidType_notOk() {
        assertThrows(NoSuchElementException.class, () -> supplier.getTable(invalidType));
    }
}