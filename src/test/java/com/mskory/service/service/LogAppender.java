package com.mskory.service.service;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import org.slf4j.LoggerFactory;

public class LogAppender {

    private final ListAppender<ILoggingEvent> logList = new ListAppender<>();

    private Logger logger;
    public LogAppender(org.slf4j.Logger logger) {
        this.logger = (Logger) logger;
        logList.setContext((LoggerContext) LoggerFactory.getILoggerFactory());
        this.logger.addAppender(logList);

    }

    public void start(){
        logList.start();
    }

    public ListAppender<ILoggingEvent> getLogList() {
        logList.list.remove(0);
        return logList;
    }

}
