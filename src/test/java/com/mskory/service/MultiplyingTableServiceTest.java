package com.mskory.service;

import ch.qos.logback.classic.spi.ILoggingEvent;

import static org.junit.jupiter.api.Assertions.*;

import com.mskory.service.service.LogAppender;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

class MultiplyingTableServiceTest {

    private static final String TEST_FORMAT_PATTERN = "0.#";
    private static final MultiplyingTableServiceSupplier tableServiceSupplier = new MultiplyingTableServiceSupplier();
    private MultiplyingTableService tableService;

    // min // max // increment // type //
    String[] shortsTest = new String[]{"170", "181", "5", "short"};
    String[] intsTest = new String[]{"3211", "4532", "479", "int"};
    String[] longsTest = new String[]{"7130", "8532", "442", "long"};
    String[] doublesTest = new String[]{"1.123113", "45.3123", "15.312", "double"};
    String[] floatsTest = new String[]{"10.3423", "15.3453", "1.9321", "float"};


    @Test
    void printTable_shortsTest_Ok() {
        assertTrue(loopTest(shortsTest[0], shortsTest[1], shortsTest[2], shortsTest[3]));
    }

    @Test
    void printTable_intsTest_Ok() {
        assertTrue(loopTest(intsTest[0], intsTest[1], intsTest[2], intsTest[3]));
    }

    @Test
    void printTable_longsTest_Ok() {
        assertTrue(loopTest(longsTest[0], longsTest[1], longsTest[2], longsTest[3]));
    }

    @Test
    void printTable_doublesTest_Ok() {
        assertTrue(loopTest(doublesTest[0], doublesTest[1], doublesTest[2], doublesTest[3]));
    }

    @Test
    void printTable_floatsTest_Ok() {
        assertTrue(loopTest(floatsTest[0], floatsTest[1], floatsTest[2], floatsTest[3]));
    }

    boolean loopTest(String minNum, String maxNum, String incrementNum, String numbersType) {
        BigDecimal min = new BigDecimal(minNum);
        BigDecimal max = new BigDecimal(maxNum);
        BigDecimal increment = new BigDecimal(incrementNum);
        BigDecimal current = new BigDecimal(minNum);
        tableService = tableServiceSupplier.getTable(numbersType);
        LogAppender logAppender = new LogAppender(tableService.getLogger());
        logAppender.start();
        tableService.printTable(minNum, maxNum, incrementNum, TEST_FORMAT_PATTERN);
        for (ILoggingEvent event : logAppender.getLogList().list) {
            BigDecimal logResult = new BigDecimal(formatString(event.toString()));
            BigDecimal result = new BigDecimal(formatString(tableService
                    .roundToString(min.multiply(current).setScale(1, RoundingMode.HALF_UP))));
            assertEquals(0, logResult.compareTo(result));
            current = current.add(increment);
            if (current.compareTo(max) > 0) {
                current = new BigDecimal(minNum);
                min = min.add(increment);
            }
        }
        return true;
    }

    private String formatString(String log) {
        return log.substring(log.lastIndexOf("=") + 1).trim().replace(",", ".");
    }
}