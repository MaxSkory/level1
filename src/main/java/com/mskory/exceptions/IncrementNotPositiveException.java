package com.mskory.exceptions;

public class IncrementNotPositiveException extends RuntimeException {
    public IncrementNotPositiveException(String message) {
        super(message);
    }
}
