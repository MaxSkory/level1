package com.mskory.exceptions;

public class NumericOverflowException extends RuntimeException {
    public NumericOverflowException(String message) {
        super(message);
    }
}
