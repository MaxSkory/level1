package com.mskory.exceptions;

public class TypeAccordanceException extends RuntimeException {
    public TypeAccordanceException(String message) {
        super(message);
    }
}
