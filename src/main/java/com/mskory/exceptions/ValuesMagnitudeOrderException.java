package com.mskory.exceptions;

public class ValuesMagnitudeOrderException extends RuntimeException {
    public ValuesMagnitudeOrderException(String message) {
        super(message);
    }
}
