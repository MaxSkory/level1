package com.mskory.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;


public class MultiplyingTableService {
    private final Logger logger = LoggerFactory.getLogger(MultiplyingTableService.class);
    private final BigDecimal numberMaxValue;
    private final String numberType;
    private DecimalFormat decimalFormat;

    public MultiplyingTableService(String numberType, BigDecimal maxValue) {
        this.numberMaxValue = maxValue;
        this.numberType = numberType;
    }

    public void printTable(String min, String max, String increment, String decimalFormatPattern) {
        Validator validator = new Validator(numberType, min, max, increment, numberMaxValue);
        validator.validate();
        BigDecimal tableMinValue = new BigDecimal(min);
        BigDecimal tableMaxValue = new BigDecimal(max);
        BigDecimal incrementValue = new BigDecimal(increment);
        decimalFormat = new DecimalFormat(decimalFormatPattern);
        printMessage(String.format("Multiplying table for %s", numberType));
        for (BigDecimal n1 = tableMinValue; n1.compareTo(tableMaxValue) <= 0; n1 = n1.add(incrementValue)) {
            for (BigDecimal n2 = tableMinValue; n2.compareTo(tableMaxValue) <= 0; n2 = n2.add(incrementValue)) {
                validator.overflowCheck(n1, n2);
                printMessage(String.format("%s * %s = %s", roundToString(n1),
                        roundToString(n2), roundToString(n1.multiply(n2))));
            }
        }
    }

    public String roundToString(BigDecimal number) {
        return decimalFormat.format(number.setScale(3, RoundingMode.HALF_UP));
    }

    // The method was created to resolve "Invoke method(s) only conditionally" problem.
    private void printMessage(String message) {
        logger.info(message);
    }

    public Logger getLogger() {
        return logger;
    }
}

