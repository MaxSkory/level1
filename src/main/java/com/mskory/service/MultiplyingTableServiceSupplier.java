package com.mskory.service;

import com.mskory.exceptions.NoSuchElementException;

import java.math.BigDecimal;
import java.util.Map;

public class MultiplyingTableServiceSupplier {
    private static final Map<String, MultiplyingTableService> types = Map.of(
            "int", new MultiplyingTableService("Integer", BigDecimal.valueOf(Integer.MAX_VALUE)),
            "double", new MultiplyingTableService("Double", BigDecimal.valueOf(Double.MAX_VALUE)),
            "float", new MultiplyingTableService("Float", BigDecimal.valueOf(Float.MAX_VALUE)),
            "long", new MultiplyingTableService("Long", BigDecimal.valueOf(Long.MAX_VALUE)),
            "short", new MultiplyingTableService("Short", BigDecimal.valueOf(Short.MAX_VALUE)));

    public MultiplyingTableService getTable(String typeName) {
        if (!types.containsKey(typeName)) {
            throw new NoSuchElementException("There is no such MultiplyingTable for numeric type \"" + typeName + "\"");
        }
        return types.get(typeName);
    }
}