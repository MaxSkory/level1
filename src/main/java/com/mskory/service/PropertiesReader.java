package com.mskory.service;

import com.mskory.exceptions.PropertiesNotFoundException;
import org.assertj.core.internal.InputStreamsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class PropertiesReader {
    private static final Logger logger = LoggerFactory.getLogger(PropertiesReader.class);
    private String fileName;


    public PropertiesReader(String fileName) {
        this.fileName = fileName;
    }

    public Properties getProperties() {
        return mergeProperties(getExternalProperties(), getJarProperties());
    }

    private Properties mergeProperties(Properties externalProperties, Properties internalProperties) {
        if (internalProperties.isEmpty() && externalProperties.isEmpty()) {
            throw new PropertiesNotFoundException("There is no properties file. " + System.lineSeparator() +
                    "The program needs at least one file of the external or internal properties to be executed.");
        } else if (!externalProperties.isEmpty()) {
            logger.info("Merging internal and external property files");
            internalProperties.putAll(externalProperties);
        }
        return internalProperties;
    }

    private String getPathToExternalProperties() {
        File file = new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
        return file.getParentFile().getPath();
    }

    private Properties getJarProperties() {
        logger.info("Reading an internal properties file");
        Properties properties = new Properties();
        try (InputStream resourceAsStream = getClass()
                .getClassLoader()
                .getResourceAsStream(fileName)) {
            if (resourceAsStream == null) {
                logger.error("There is no internal properties file {}", fileName);
                return properties;
            }
            InputStreamReader reader = new InputStreamReader(resourceAsStream);
            properties.load(reader);
            reader.close();
            return properties;
        } catch (IOException e) {
            throw new InputStreamsException("Can't read " + fileName + " from jar.");
        }
    }

    private Properties getExternalProperties() {
        logger.info("Reading an external properties file");
        String path = getPathToExternalProperties();
        Properties properties = new Properties();
        try (FileReader reader = new FileReader(path + File.separator
                + fileName, StandardCharsets.UTF_8)) {

            properties.load(reader);
            return properties;
        } catch (IOException e) {
            logger.info(String.format("The external properties file %s%s not found%s" +
                            "The program will be executed with an internal properties file",
                    path, fileName, System.lineSeparator()));
        }
        return properties;
    }

    public PropertiesReader setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public Logger getLogger(){
        return logger;
    }
}
