package com.mskory.service;

import com.mskory.exceptions.TypeAccordanceException;
import com.mskory.exceptions.NumericOverflowException;
import com.mskory.exceptions.ValuesMagnitudeOrderException;
import com.mskory.exceptions.IncrementNotPositiveException;

import java.math.BigDecimal;

public class Validator {
    public static final String DECIMAL_DELIMITER = ".";
    public static final String NUMBER_FORMAT_PATTERN = "-?\\d+(.\\d+)?";
    private String min;
    private String max;
    private String increment;
    private BigDecimal maxValue;
    private String numberType;

    public Validator(String numberType, String min, String max, String increment, BigDecimal maxValue) {
        this.min = min;
        this.max = max;
        this.increment = increment;
        this.maxValue = maxValue;
        this.numberType = numberType;
    }

    public void overflowCheck(BigDecimal firstOperand, BigDecimal secondOperand) {
        BigDecimal result = firstOperand.multiply(secondOperand);
        if (result.compareTo(maxValue) >= 0 || result.abs().compareTo(maxValue.subtract(BigDecimal.valueOf(1))) >= 0) {
            throw new NumericOverflowException("The result " + firstOperand + " * " + secondOperand +
                    " is bigger than the program can handle with. The max value for " + numberType +
                    " is " + maxValue);
        }
    }

    public boolean validate() {
        nullCheck();
        numbersFormatCheck();
        zeroIncrementCheck();
        magnitudeOrderCheck();
        typeAccordanceCheck();
        return true;
    }

    private void nullCheck() {
        if (min == null || max == null || increment == null) {
            throw new NullPointerException("Any of the processed values can't be null " + getErrorMessageWithParameters());
        }
    }

    private void magnitudeOrderCheck() {
        if (new BigDecimal(min).compareTo(new BigDecimal(max)) > 0) {
            throw new ValuesMagnitudeOrderException("The parameter min value can't be greater than max value." +
                    System.lineSeparator() + getErrorMessageWithParameters());
        }
    }

    private void zeroIncrementCheck() {
        if (new BigDecimal(increment).signum() <= 0) {
            throw new IncrementNotPositiveException("The increment parameter can't be zero" +
                    getErrorMessageWithParameters());
        }
    }

    private void typeAccordanceCheck() {
        if ((min.contains(DECIMAL_DELIMITER) || max.contains(DECIMAL_DELIMITER)
                || increment.contains(DECIMAL_DELIMITER)) && !maxValue.toString().contains(DECIMAL_DELIMITER)) {
            throw new TypeAccordanceException(getErrorMessageWithParameters() + " as " + numberType);
        }
    }

    private void numbersFormatCheck() {
        if (!min.matches(NUMBER_FORMAT_PATTERN) ||
                !max.matches(NUMBER_FORMAT_PATTERN) ||
                !increment.matches(NUMBER_FORMAT_PATTERN)) {
            throw new NumberFormatException("Wrong format for " + numberType +
                    System.lineSeparator() + getErrorMessageWithParameters());
        }
    }

    private String getErrorMessageWithParameters() {
        return String.format("Can't process parameters: ||min = %s||max = %s||increment = %s||", min, max, increment);
    }

    public Validator setMin(String min) {
        this.min = min;
        return this;
    }

    public Validator setMax(String max) {
        this.max = max;
        return this;
    }

    public Validator setIncrement(String increment) {
        this.increment = increment;
        return this;
    }

    public Validator setMaxValue(BigDecimal maxValue) {
        this.maxValue = maxValue;
        return this;
    }

    public Validator setNumberType(String numberType) {
        this.numberType = numberType;
        return this;
    }
}
