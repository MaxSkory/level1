package com.mskory;

import com.mskory.service.MultiplyingTableServiceSupplier;
import com.mskory.service.PropertiesReader;

import java.util.Properties;

public class App {
    public static final String DEFAULT_NUMBERS_TYPE = "int";
    public static final String PROPERTIES_FILE_NAME = ".properties";
    public static final String MIN_VALUE_NAME = "min";
    public static final String MAX_VALUE_NAME = "max";
    public static final String INCREMENT_NAME = "increment";
    public static final String NUMBERS_TYPE_NAME = "type";
    public static final String DECIMAL_FORMAT_PATTERN = "0.###";

    public static void main(String[] args) {
        PropertiesReader propertiesReader = new PropertiesReader(PROPERTIES_FILE_NAME);
        Properties properties = propertiesReader.getProperties();
        String numbersType = getNumbersType();
        String minValue = formatValue(properties.getProperty(MIN_VALUE_NAME));
        String maxValue = formatValue(properties.getProperty(MAX_VALUE_NAME));
        String increment = formatValue(properties.getProperty(INCREMENT_NAME));
        MultiplyingTableServiceSupplier supplier = new MultiplyingTableServiceSupplier();
        supplier.getTable(numbersType).printTable(minValue, maxValue, increment, DECIMAL_FORMAT_PATTERN);
    }

    private static String formatValue(String property) {
        return property.trim().replace(",", ".");
    }

    private static String getNumbersType() {
        return System.getProperty(NUMBERS_TYPE_NAME) == null ?
                DEFAULT_NUMBERS_TYPE : System.getProperty(NUMBERS_TYPE_NAME);
    }
}
